<?php

/**
 * Book Model Class
 *
 * Prepare and execute queries with received parameter and returns it to the BookController.
 * Called from BookController (book-controller.php) by appropriate method.
 */

class BookModel
{
    /**
     * Checks parameters and prepare query to find existing book/books.
     *
     * @param
     *      array $param - book id, to find specific book or NULL to search all books.
     *      NULL $param - search for all existent books.
     *
     * @return array - search result (list of existing books)
     */
    public function get( $param = null )
    {
        $filterQuery = "";
        if ( $param != null ) {
            $filterQuery = ' WHERE ';
            foreach ( $param as $key => $value ) {
                if ( $key != 0 ) {
                    $filterQuery .= ' AND ';
                }
                $filterQuery .= $value["field"] . " = " . "'" . $value["value"] . "' ";
            }
        }
        $pdo = new PdoMysql();
        return $pdo->read("SELECT * FROM books" . $filterQuery . " ORDER BY id DESC", $param);
    }

    /**
     * Adds a book with it's parameters to DB.
     *
     * @param array $param - book field values to be set for a new book.
     * @return int - id of added book.
     */
    public function add ($param) {
        $pdo = new PdoMysql();
        return $pdo->create("INSERT INTO books (title, author, year, isbn, editor, evaluation)
                             VALUES ( :title, :author, :year, :isbn, :editor, :evaluation );", $param);
    }

    /**
     * Edit existing book fields.
     *
     * @param array $param - fields to set for a new book in DB : id, title, author, year, isbn, editor, evaluation.
     * @return int - number of affected rows.
     */
    public function edit ($param) {
        $pdo = new PdoMysql();
        return $pdo->update("UPDATE books SET  
                                    title = :title, 
                                    author = :author, 
                                    year = :year, 
                                    isbn = :isbn,
                                    editor = :editor,
                                    evaluation = :evaluation
                                    WHERE id = :id;", $param);
    }

    /**
     * Removes a book from DB.
     *
     * @param array $param - id of a book.
     * @return int - number of affected rows.
     */
    public function remove ($param) {
        $pdo = new PdoMysql();
        return $pdo-> delete( "DELETE FROM books WHERE id = :bookId", $param);
    }


    /**
     * Searches data for specific column
     * Used for autocomplete functionality
     *
     * @param $param array - column name
     * @return array - found values
     */
    public function autocomplete( $param )
    {
        $pdo = new PdoMysql();
        return $pdo->read("SELECT ". $param[0] . " FROM books GROUP BY " .  $param[0], $param );
    }
}