<?php

/**
 * PdoMysql Class
 * PDO Model
 *
 * Treats/filters received variables and executes queries.
 * Called on query executions.
 */

// Import data base configurations
require_once "config.php";

class PdoMysql
{
    private $pdo;
    private $stmt;

    /**
     * Establishes connection to DB using parameters from configuration file.
     *
     * @return void
     */
    function __construct()
    {
        try
        {
            // Connect to DB
            $options = [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ];
            $this->pdo = new PDO("mysql:host=" . HOST . ";dbname=" . DB . ";charset=utf8", USERNAME, PASS, $options);
        }
        // On message - show message
        catch (PDOException $e)
        {
            die('Connection message: ' . $e->getMessage());
        }
    }

    /**
     * Submits a SQL query.
     *
     * @param string $query - SQL query.
     * @param array $param - parameters for query.
     *
     * @return void
     */
    public function submit($query, $param)
    {
        $this->stmt = $this->pdo->prepare($query);
        $this->stmt->execute($param);
    }


    // CRUD Operation Methods

    /**
     * Submits a SQL query that creates a new objects in DB.
     * Applies parameter for a query.
     *
     * @param string $query - SQL query.
     * @param array $param - query parameters.
     *
     * @return int - id of last inserted row.
     */
    public function create($query, $param)
    {
        $this->submit($query, $param);
        return $this->pdo->lastInsertId();
    }

    /**
     * SELECTs data from DB and returns it.
     *
     * @param string $query - SQL query.
     *
     * @return array $resTab - Array of PHP objects.
     */
    public function read($query, $param)
    {
        $this->submit($query, $param);
        $resTab = [];
        // Pass through received SQL objects and transforms them into PHP objects
        while ($res = $this->stmt->fetch()) {
            $resTab[] = $res;
        }
        return $resTab;
    }

    /**
     * Updates existing objects in DB.
     * Uses $param to identify needed field.
     *
     * @param string $query - SQL query.
     * @param array $param - Query parameters.
     *
     * @return int - Number of affected rows.
     */
    public function update($query, $param)
    {
        $this->submit($query, $param);
        return $this->stmt->rowCount();
    }

    /**
     * Deletes an objects from DB.
     * Uses $param to identify needed field.
     *
     * @param string $query - SQL query.
     * @param array $param - Query parameters.
     *
     * @return int - Number of affected rows.
     */
    public function delete($query, $param)
    {
        $this->submit($query, $param);
        return $this->stmt->rowCount();
    }
}