<?php
/**
 * Created by Peter Yablochkin.
 * Date: 1-Aug-18
 * Time: 22:20
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta charset="UTF-8">
    <title>Library</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="res/css/stylesheet.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">

    <!--    jQuery 3.3.1 slim   -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <!--    jQuery UI 1.12.1  -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <script defer src="res/js/javaScript_v2.js"></script>
</head>
<body>
    <header>
        Library
    </header>
<!--    Nav Menu-->
    <nav class="container mt-5 mb-4">
        <button id="add-new-book-btn"><i class="fas fa-plus"></i>Add a book</button>
        <button id="show-all-books-btn"><i class="fas fa-list-ul"></i>Show all books</button>
    </nav>
    <!--    End Nav Menu-->

    <main class="container">
<!--        Add book form-->
        <section class="mt-5 mb-4">
            <table class="table table-sm table-light" id="table-add-book">
                <thead>
                <tr>
                    <th class="text-center" colspan="7" scope="col">Add a book</th>
                </tr>
                </thead>
                <tbody id="book-add-table">
                    <tr>
                        <td><input type="text" class="form-control" name="add-book-title" placeholder="Title"></td>
                        <td><input type="text" class="form-control" name="add-book-author" placeholder="Author"></td>
                        <td><input type="text" class="form-control" name="add-book-year" placeholder="Year"></td>
                        <td><input type="text" class="form-control" name="add-book-isbn" placeholder="ISBN"></td>
                        <td><input type="text" class="form-control" name="add-book-editor" placeholder="Editor"></td>
                        <td><input type="text" class="form-control" name="add-book-evaluation" placeholder="Evaluation"></td>
                        <td class="text-center align-middle"><button class="btn-add no-border-btn" id="add-book-btn"><i class="fas fa-plus"></i></button></td>
                    </tr>
                </tbody>
            </table>
        </section>
        <!--       End Add book form-->

        <!--        Search Form-->
        <section>
            <table class="table table-sm table-light" id="table-search-book">
                <thead>
                <tr>
                    <th class="text-center" colspan="7" scope="col">Search a book</th>
                </tr>
                </thead>
                <tbody id="book-search-result-table">
                <tr id="search-row">
                    <td><input type="text" class="form-control auto-complete" name="title" id="autocomplete-title" placeholder="Title"></td>
                    <td><input type="text" class="form-control auto-complete" name="author" id="autocomplete-author" placeholder="Author"></td>
                    <td><input type="text" class="form-control auto-complete" name="year" id="autocomplete-year" placeholder="Year"></td>
                    <td><input type="text" class="form-control auto-complete" name="isbn" id="autocomplete-isbn" placeholder="ISBN"></td>
                    <td><input type="text" class="form-control auto-complete" name="editor" id="autocomplete-editor" placeholder="Editor"></td>
                    <td><input type="text" class="form-control auto-complete" name="evaluation" id="autocomplete-evaluation" placeholder="Evaluation"></td>
                    <td class="td-btn"><button class="no-border-btn search-btn" id="search-btn"><i class="fas fa-search"></i></button></td>
                </tr>
                </tbody>
            </table>
        </section>
        <!--       END Search Form-->

        <!--        Main table for book prints-->
        <section>
            <table class="table table-sm table-striped" id="table-show-all-books">
                <thead>
                <tr>
                    <th class="text-center" colspan="8" scope="col">All books</th>
                </tr>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Author</th>
                    <th scope="col">Year</th>
                    <th scope="col">ISBN</th>
                    <th scope="col">Editor</th>
                    <th scope="col">Evaluation</th>
                    <th scope="col"></th>
                </tr>
                </thead>
<!--                Table body for book inserts-->
                <tbody id="book-table"></tbody>
            </table>
        </section>
        <!--        END Main table for book prints-->
    </main>
</body>
</html>