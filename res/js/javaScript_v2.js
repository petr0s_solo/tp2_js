/**
 * Library management script (client)
 * Author: Peter Yablochkin
 * Created: 1 Aug 2018
 * Project: TP 2 JavaScript Course
 */



/*************** Class Creation ***************/

/**
 * Book class
 * Used for book instance creation
 * Called on book add/update features
 */
class Book {
    /**
     * constructor
     * @param obj - book with necessary fields
     */
    constructor ( obj ) {
        if(obj.id) {
            this.id = obj.id;
        }
        this.title = obj.title;
        this.author = obj.author;
        this.year = obj.year;
        this.isbn = obj.isbn;
        this.editor = obj.editor;
        this.evaluation = obj.evaluation;
    };

    /**
     * checkBookInfo
     * Used to verify book info entered by user;
     * @returns {boolean}
     */
    checkBookInfo ( ) {
        message = [];
        if(this.id) {
            if(isNaN(this.id)) {
                message.push('ID message');
            }
        }
        if(isNaN(this.title) === false || this.title.length < 1) {
            message.push('Title field should consist from letter and can not be empty');
        }
        if(isNaN(this.author) === false || this.author.length < 1) {
            message.push('Author field should consist from letter and can not be empty');
        }
        if(isNaN(this.editor) === false || this.editor.length < 1) {
            message.push('Editor field should consist from letter and can not be empty');
        }
        if(isNaN(this.year) === true || this.year.length > 4 || this.year.length < 1 || this.year > (new Date().getFullYear())) {
            message.push('Year field should consist from numbers, can not be empty and could not be more recent than actual year');
        }
        if(isNaN(this.evaluation) === true || this.evaluation.length < 1 || this.evaluation > 10) {
            message.push('Evaluation field should consist from letter and can not be empty');
        }

        // If all info is ok
        if(message.length === 0) {
            return true;
        } else {
            printMessage('error');
        }
    }
}

/**
 * Library class
 * Used to manage library by CRUD methods
 * Called on every manipulation with library DB
 */
class Library {
    constructor( name ) {
        this.name = name;
    }

    /**
     * read method
     * Used to get books from DB
     *
     * @param filter - optional value. Is used to search by specific data (user search)
     */
    read ( filter ) {
        let self = this; // binding method to call in jquery function
        $.get(
            window.location.href + 'index.php',
            {
                filter: filter, // search data
                action: 'getBook' // controller action
            }
        ).done( function( data ) {
            // calling print method
            self.printBooks( JSON.parse(data), 'book-table' );
        }).fail(function() {
            // Pushing message
            message.push('Read error');
            // Calling message print and specifying message type
            printMessage('error');
        });
    }

    /**
     * create method
     * Adds a new book in library
     *
     * @param book obj - book instance
     */
    create ( book ) {
        let self = this; // binding method to call in jquery function
        $.get(
            window.location.href + 'index.php',
            {
                filter: book, // book instance
                action: 'addBook' // controller action
            }
        ).done( function( data ) {
            // Calling read books method
            self.read();
            // Pushing message
            message.push('Book was created successfully');
            // Calling message print and specifying message type
            printMessage('success');
        }).fail(function () {
            // Pushing message
            message.push('Create error');
            // Calling message print and specifying message type
            printMessage('error');
        });
    }

    /**
     * delete method
     * Deletes a book from library
     * @param id int - book id
     */
    delete ( id ) {
        let self = this;
        $.get(
            window.location.href + 'index.php',
            {
                filter: id, // book id
                action: 'removeBook' // controller action
            }
        ).done( function( data ) {
            // Calling read books method
            self.read();
            // Pushing message
            message.push('Book was deleted successfully');
            // Calling message print and specifying message type
            printMessage('success');
        }).fail(function() {
            // Pushing message
            message.push('Delete error');
            // Calling message print and specifying message type
            printMessage('error');
        });
    }

    /**
     * update method
     * Updates book info
     *
     * @param data obj - book instance
     */
    update ( data ) {
        let self = this;
        $.get(
            window.location.href + 'index.php?action=editBook',
            { filter: data }
        ).done( function( data ) {
            // Calling read books method
            self.read();
            // Pushing message
            message.push('Book was updated successfully');
            // Calling message print and specifying message type
            printMessage('success');
        }).fail(function() {
            // Pushing message
            message.push('Update message');
            // Calling message print and specifying message type
            printMessage('error');
        });
    }

    /**
     * printBooks method
     * Used to add and print books to the table
     *
     * @param books array - array of books
     * @param tableId string - table id where data should be inserted
     */
    printBooks ( books, tableId ) {
        tableId = '#' + tableId; // Adding symbol for js search query
        $(tableId).empty(); // Removing all data from the table
        for (let i = 0; i < books.length; i++) {
            // Creating table rows and appending needed fields
            let row = $('<tr>', { 'id': 'book-' + books[i].id });
            $('<th>', {'scope': 'row', text: i+1}).appendTo(row);
            $('<td>', {text: books[i].title, class: 'title'}).appendTo(row);
            $('<td>', {text: books[i].author, class: 'author'}).appendTo(row);
            $('<td>', {text: books[i].year, class: 'year'}).appendTo(row);
            $('<td>', {text: books[i].isbn, class: 'isbn'}).appendTo(row);
            $('<td>', {text: books[i].editor, class: 'editor'}).appendTo(row);
            $('<td>', {text: books[i].evaluation, class: 'evaluation'}).appendTo(row);
            $('<th>', {'class': 'text-center'}).append('<button class="no-border-btn remove-btn"><i class="fas fa-trash"></i></button>').appendTo(row);
            // Appending row to the table
            row.appendTo( tableId );
        }
    }
}
/*************** End Class Creation ***************/


// Declaring needed/helping variables
let table = $( "#table-show-all-books" ); // main table
let bookTable = $( '#book-table' ); // table's tbody to insert books
let library = new Library ('New Library'); // Declaring library instance
let message = []; // Message array for messages

/*************** Button Event Binds ***************/
// Show book ADD FORM button event
$("#add-new-book-btn").click(function () {
    $("#table-add-book").toggle("blind", 300);
});

// ADD a book event
$('#add-book-btn').click(
    function () {
        let addBookInputs = $('#book-add-table input');
        // Creating Book instance with filled user data
        let newBook = new Book ({
            title: addBookInputs[0].value,
            author: addBookInputs[1].value,
            year: addBookInputs[2].value,
            isbn: addBookInputs[3].value,
            editor: addBookInputs[4].value,
            evaluation: addBookInputs[5].value
        });
        // Checking if Book data is OK
        if(newBook.checkBookInfo() === true) {
            // Creating a new book in the Library
            library.create( newBook );
            // Clearing input fields
            $(addBookInputs).each( function ( key, value ) {
                value.value = '';
            })
        }
    }
);

// Show all books button click event
$('#show-all-books-btn').click(
    function () {
        //Managing animations
        if (table.css('display') === 'table' ) {
            // Getting table off
            table.hide( "drop", { direction: "left" }, 400 );
        }
        setTimeout(function(){
            // Reading data from DB
            library.read();
            // Getting table on
            table.show( "drop", { direction: "left" }, 400 );
        }, 400);
    }
);

// Search book event
$('#search-btn').click( function () {
    //Managing animations
    if (table.css('display') === 'table' ) {
        // Getting table off
        table.hide( "drop", { direction: "right" }, 400 );
    }
    setTimeout(function(){
        // Reading data from DB
        library.read( getInputValues('book-search-result-table') );
        // Getting table on
        table.show( "drop", { direction: "right" }, 400 );
    }, 400);
});

// Transform Input on book edit (called on td click)
$(bookTable).on('click', 'td',  function () {
    $(this).each( function ( index, value ) {
        if ($(value).find('input').length === 0 ) {
            let val = value.innerHTML; // passing td value
            value.innerHTML = ''; // clearing td
            // Creating an input with td data
            $('<input>').attr({
                type: 'text',
                class: 'form-control form-control-sm text-center modif-input',
                name: $(value).attr('class'),
                value: val
            }).appendTo(value);
        }
    });
});


// Update book in the Library event (On MOUSE OUT event)
$(bookTable).on('focusout', '.modif-input', function () {
    // Gathering all tds from focused row
    let tds = $(this).parent().parent().find('td');
    let book = Object;
    // Adding id to the object
    book['id'] = getBookId(tds.parent().attr('id'));
    // Looping through tds to gather book info
    $(tds).each( function ( key, value ) {
        // if td contains an input
        if ($(value).find('input').length === 1 ) {
            // Binding object field name
            let fldName = $(value).find('input').attr('name');
            // Binding input value
            let fldVal = $(value).find('input').val();
            // Adding data to the object
            book[fldName] = fldVal;
        } else {
            // Binding object field name
            let fldName = $(value).attr('class');
            // Binding value
            let fldVal = $(value).text();
            // Adding new value to the object
            book[fldName] = fldVal;
        }
    });
    // Creating new book instance with gathered data
    let editedBook = new Book({
        id: book.id,
        title: book.title,
        author: book.author,
        year: book.year,
        isbn: book.isbn,
        editor: book.editor,
        evaluation: book.evaluation
    });
    // Verifying book info
    if(editedBook.checkBookInfo() === true) {
        // Updating the book in the library
        library.update( editedBook );
    }
});

// Remove a book event
$(bookTable).on('click', '.remove-btn', function () {
    // Searching and binding book id
    let bookId = getBookId($(this).parent().parent().attr('id'));
    // Executing delete function
    library.delete(bookId);
});

/**
 * printMessage function
 * Prints messages
 *
 * @param data string - message type (error/succes)
 * (error type bind a red text color, success - bind a green color)
 */
function printMessage ( data ) {
    let msgBlock = $('#message-block');
    // Transferring messages to new variable
    let msg = message;
    // Clearing data from messages
    message = [];

    // Checking if message block already exists
    if(msgBlock.length) {
        // removing
        msgBlock.remove();
    }
    // Creating new message section
    let messageDiv = $('<section>', {id: 'message-block', class: data + ' text-center'});
    let msgUl = $('<ul>');
    for(let i = 0; i < msg.length; i++ ) {
        // Appending messages to li
        msgUl.append('<li>' + msg[i] + '</li>');
        messageDiv.append(msgUl);
        // appending to the main block
        $(messageDiv).appendTo('main');
    }
    setTimeout(function(){
        // removing message block with timeout
        $('#message-block').remove();
    }, 5000);
}

/**
 * getBookId
 * Parses a string for book id
 * Used in book delete
 *
 * @param str - row id
 * @returns int - book id
 */
function getBookId ( str ) {
    const regex = /[0-9]+/g;
    return str.match(regex)[0];
}

/**
 * getInputValues
 * Loops through dom elements(td) and gathers entered values
 * @param blockId - id of needed block
 * @returns searchFilter array - with fields names and its values
 */
function getInputValues ( blockId ) {
    let inputs = $('#' + blockId).find('input');
    let searchFilter = Array();
    $(inputs).each( function ( index, value ) {
        if( $(value).val() !== '' ){
            searchFilter.push({
                field: $(value).attr('name'),
                value: $(value).val()
            })
        }
    });
    return searchFilter;
}
/*************** END Button Event Binds ***************/


/*************** Jquery Autocomplete functionality ***************/
/**
 * autoComplete
 * Used to search column values in the library and to prepare data for autocomplete
 * @param fieldName string - search form input fieldname
 */
function autoComplete(fieldName) {
    $.ajax({
        url: window.location.href + 'index.php',
        dataType: "json",
        data: {
            filter: fieldName, // defining data for the query
            action: 'autocomplete' // defining controller action
        },
        success: function (data) {
            // Calling jquery autocomplete
            $("#autocomplete-" + fieldName).autocomplete({
                source: data // specifying data source
            });
        },
        error: function () {
            // Pushing message
            message.push('Autocomplete message');
            // Calling message print and specifying message type
            printMessage('error');
        }
    });
}

// Binding autocomplete event on form focus
$('.auto-complete').each( function (key, value) {
    $(value).focus( function () {
       autoComplete($(value).attr('name'));
    });
});
/********* END AUTO COMPLETE **********/

