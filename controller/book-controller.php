<?php

/**
 * Book Controller Class
 *
 * Manages received data from router.
 * Transforms and prepares data for BookModel before query execution.
 * Called from router (index.php).
 *
 */

class BookController
{
    /**
     * Checks and searches a specific book if $param is sent from router otherwise
     * searches all existing books in DB.
     *
     * Called by getBook action.
     *
     * @param
     *      array $param - book id, to find specific book or NULL to search all books.
     *      NULL $param - search for all existent books.
     *
     * @return array - search result (list of existing books)
     */
    public function get($param = null)
    {
        $bookModel = new BookModel();
        return $bookModel->get($param);
    }

    /**
     * Adds a book with it's parameters to DB.
     *
     * Called by addBook action.
     *
     * @param array $param - fields to set for a new book in DB : titre, auteur, annee, isbn, editeur, evaluation.
     * @return int - id of added book.
     */
    public function add($param)
    {
        $bookModel = new BookModel();
        return $bookModel->add($param);
    }

    /**
     * Edit existing book values.
     *
     * Called by editBook action.
     *
     * @param array $param - parameters to set for a new book : id, titre, auteur, annee, isbn, editeur, evaluation.
     * @return int - number of affected rows.
     */
    public function edit($param)
    {
        $bookModel = new BookModel();
        return $bookModel->edit($param);
    }

    /**
     * Removes a book from DB.
     *
     * Called by removeBook action.
     *
     * @param array $param - id of existing book.
     * @return int - number of affected rows.
     */
    public function remove($param)
    {
        $bookModel = new BookModel();
        return $bookModel->remove($param);
    }

    /**
     * Searches data for specific column
     * Used for autocomplete functionality
     *
     * @param $param array - column name
     * @return array - found values
     */
    public function autocomplete ($param)
    {
        $bookModel = new BookModel();
        return $bookModel->autocomplete($param);
    }
}