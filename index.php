<?php
/**
 * Created by Piotr Iablocichin.
 * Groupe: 15628
 * Date: 18-Jul-18
 */

/**
 *  Router.
 *  Treats clients queries and forwards them to appropriate methods.
 */

// Import files
require_once "controller/book-controller.php";
require_once("model/book-model.php");
require_once "model/pdo-mysql.php";

// Identify query action
if(isset($_REQUEST["action"]))
{
    $action = $_REQUEST["action"];
}
else
{
    $action = "home";
}

// Forward to appropriate action
switch($action)
{
    case "home":
//        echo "home <br>";
        require_once "res/view/home.php";
        break;

    // Show all books and search book action
    case "getBook":
        $filter = Array();
        if( isset($_GET['filter']) ) {
            $filter = $_GET['filter'];
        }

        $book = new BookController();
        $test = json_encode($book->get($filter));
        echo json_encode($book->get($filter));
        break;

    // Add a book action
    case "addBook":
        $fields = array(
            'title' => $_GET['filter']['title'],
            'author' =>  $_GET['filter']['author'],
            'year' =>  $_GET['filter']['year'],
            'isbn' =>  $_GET['filter']['isbn'],
            'editor' =>  $_GET['filter']['editor'],
            'evaluation' =>  $_GET['filter']['evaluation']
        );
        $book = new BookController();
        echo $book->add($fields);
        break;

    // Edit a book action
    case "editBook":
        $params = array(
            'id' => $_GET['filter']['id'],
            'title' => $_GET['filter']['title'],
            'author' =>  $_GET['filter']['author'],
            'year' =>  $_GET['filter']['year'],
            'isbn' =>  $_GET['filter']['isbn'],
            'editor' =>  $_GET['filter']['editor'],
            'evaluation' =>  $_GET['filter']['evaluation']
        );
        $book = new BookController();
        echo $book->edit($params);
        break;

    // Remove a book
    case "removeBook":
        $param['bookId'] = $_GET['filter'];
        $book = new BookController();
        echo $book->remove($param);
        break;

    // Autocomplete action
    case "autocomplete":
        $book = new BookController();
        $param = Array($_GET['filter']);
        $queryResult = $book->autocomplete($param);
        $result = [];
        $value = $param[0];
        foreach ($queryResult as $key => $obj ) {
            $result[] = $obj->$value;
        }
        echo json_encode($result);
        break;
    default:
        echo "(_x_)";
}